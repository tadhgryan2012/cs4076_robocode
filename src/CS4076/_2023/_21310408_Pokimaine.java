package CS4076._2023;

import robocode.*;
import static robocode.util.Utils.normalRelativeAngle;

public class circlesV2 extends AdvancedRobot {
	public void run() {
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForRobotTurn(true);
		setAdjustRadarForGunTurn(true);

		while (true) {
			doMove();
			doScan();
			execute();
		}
	}

	private void doScan() {
		setTurnRadarRightRadians(360);
	}

	private void doFire(ScannedRobotEvent target) {
		double power = (getEnergy()/100) * 3;
		if (getEnergy() <= 20)
			power = 0.1;

		double distance = target.getDistance();
		double angle = getHeadingRadians() + target.getBearingRadians();
		double velocity = target.getVelocity();
		double heading = target.getHeadingRadians();

		double scannedX = getX() + (Math.sin(angle) * distance);
		double scannedY = getY() + (Math.cos(angle) * distance);

		double speedOfBullet = 20 - (power * 3);

		int numberOfTurns = (int) Math.round(distance / speedOfBullet);

		double predictedX = scannedX + (numberOfTurns * velocity * Math.sin(heading));
		double predictedY = scannedY + (numberOfTurns * velocity * Math.sin(heading));

		turnGunRightRadians(normalRelativeAngle(Math.atan2(predictedX - getX(), predictedY - getY()) - getGunHeadingRadians()));
		
		waitFor(new GunTurnCompleteCondition(this));

		fire(power);
	}

	private void doMove() {
		if (!moveOutOfBorder()) {
			setAhead(Double.POSITIVE_INFINITY);
			setTurnLeft(Double.POSITIVE_INFINITY);
		}
	}

	private boolean moveOutOfBorder() {
		boolean shouldMove = false;

		if (getSentryBorderSize() >= getX() && getBattleFieldWidth() - getSentryBorderSize() < getX())	// At Left
			shouldMove = true;
		if (getBattleFieldWidth() - getSentryBorderSize() >= getX() && getSentryBorderSize() < getX())	// At Right
			shouldMove = true;
		if (getSentryBorderSize() >= getY() && getBattleFieldHeight() - getSentryBorderSize() > getY()) // At Bottom
			shouldMove = true;
		if (getBattleFieldHeight() - getSentryBorderSize() >= getY() && getSentryBorderSize() < getY())	// At Top
			shouldMove = true;

		if (shouldMove) {
			double centreX = getBattleFieldWidth() / 2;
			double centreY = getBattleFieldHeight() / 2;
	
			setTurnRightRadians(normalRelativeAngle(Math.atan2(centreX - getX(), centreY - getY()) - getHeadingRadians()));
			setAhead(getSentryBorderSize());
		}
		return shouldMove;
	}

	public void onScannedRobot(ScannedRobotEvent robot) {
		doMove();
		if (!robot.isSentryRobot())
			doFire(robot);
	}

	public void onHitRobot(HitRobotEvent e) {
		double turnGunAmt = normalRelativeAngle(e.getBearingRadians() + getHeadingRadians() - getGunHeadingRadians());
		turnGunRight(turnGunAmt);
		fire(3);
	}
	
	public void onWin(WinEvent e) {
        for (int i = 0; i < 50; i++) {
            turnRight(30);
            turnLeft(30);
        }
    }
}